<?php
$layouts = array (
);

if (have_rows('flex_content_layouts')) {
    while (have_rows('flex_content_layouts')) {
        the_row();
        foreach ($layouts as $layout) {
            if (get_row_layout() == $layout) {
                include __DIR__ . '/flex-content/' . $layout . '.php';
            }
        }
    }
}