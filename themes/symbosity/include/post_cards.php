<?php
function make_post_card($post, $classes = '') { 
    $id = $post->ID;
    $post_type = $post->post_type;
    $post_type_obj = get_post_type_object($post_type);

    $card_classes = 'post-card ' . $classes . ' ' . $post_type;

    $link = '';
    $target = '';
    $image = get_the_post_thumbnail_url($id);
    $topic = '';
    $desc = '';
    $category = '';

    if (!$image) {
        $image = get_field('placeholder', 'options')['url'];
        $card_classes .= ' placeholder';
    }

    if ($post_type == 'articles') {
        $link = get_the_permalink($id);
        $target = "_self";
        $category = get_field('article_category');
    } elseif ($post_type == 'videos') {
        $link = get_field('video_link', $id, false);
        $target="_blank";
    } elseif ($post_type == 'resources') {
        $link = get_field('resource_link', $id);
        $target="_blank";
        $topic = get_field('resource_topic');
        $desc = get_field('resource_description');
        $category = get_field('resource_category');
    }

    if ($category) {
        $card_classes .= ' category-' . $category->slug;
    }
?>
<a class="<?php echo $card_classes; ?>" href="<?php echo $link; ?>" target="<?php echo $target; ?>">
    <div class="post-card-top">
        <?php if ($post_type != 'resources') : ?>
        <img class="post-card-top-image" src="<?php echo $image; ?>">
        <?php endif; ?>
        <div class="post-card-top-play">
            <i class="fas fa-play post-card-top-play-icon"></i>
        </div>
    </div>
    <div class="post-card-bottom">
        <p class="post-card-legend">
            <?php echo $post_type_obj->label; ?>
            <?php if ($category) { echo ' / ' . $category->name; } ?>
            <?php if ($topic) { echo ' / ' . $topic->name; } ?>
        </p>
        <p class="post-card-title"><?php echo get_the_title($id); ?></p>
        <?php if ($desc) : ?>
        <p class="post-card-description"><?php echo $desc; ?></p>
        <?php endif; ?>
    </div>
</a>
<?php
}
?>