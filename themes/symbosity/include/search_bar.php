<form class="search-bar" action="<?php echo get_site_url(); ?>">
    <input class="search-input" placeholder="Search here..." type="search" name="s">
    <button class="search-button" type="submit">
        <i class="fas fa-search"></i>
    </button>
</form>