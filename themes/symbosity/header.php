<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport"
        content="width=device-width, initial-scale=1, shrink-to-fit=no, maximum-scale=1, user-scalable=0" />
    <title><?php bloginfo('name'); ?></title>
    <?php wp_head(); ?>
</head>

<?php global $pageID; ?>
<?php if (!isset($pageID)) { $pageID = $post->ID; } ?>
<?php include __DIR__ . '/include/post_cards.php'; ?>

<body <?php body_class(); ?>>
    <div class="popup">
        <div class="popup-inner">
            <button type="button" class="popup-close">
                <i class="fas fa-times"></i>
            </button>
            <?php $form = get_field('subscribe_form', 'options'); ?>
            <?php echo do_shortcode('[mc4wp_form id="' . $form->ID . '"]'); ?>
        </div>
    </div>
    <div class="navbar">
        <nav class="navbar-menu">
            <?php wp_nav_menu(array('theme_location' => 'main_menu', 'menu_class' => 'navbar-menu-list', 'container' => false)); ?>
            <?php include __DIR__ . '/include/search_bar.php'; ?>
        </nav>
        <div class="navbar-main container full">
            <a class="navbar-logo" href="<?php echo get_site_url(); ?>">
                <?php if ($img = get_field('logo', 'options')) : ?>
                <img class="navbar-logo-image" src="<?php echo $img['url']; ?>">
                <?php endif; ?>
                <p class="navbar-logo-text">Symbosity</p>
            </a>
            <?php include __DIR__ . '/include/search_bar.php'; ?>
            <button class="hamburger hamburger--spin" type="button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>

    <?php if (!is_front_page()) : ?>
    <header class="header">
        <div class="header-background header-background-pattern"></div>
        <?php
        $img = '';

        if ($field = get_field('header_background_image', $pageID)['url']) {
            $img = $field;
        }
        elseif (is_singular()) {
            $img = get_the_post_thumbnail_url();
        }
        ?>
        <?php if ($img) : ?>
        <img class="header-background header-background-image" src="<?php echo $img; ?>">
        <div class="header-background header-background-sheen"></div>
        <?php endif; ?>
        <div class="header-content container">
            <div class="header-content-inner">
                <?php if (is_singular('articles')) : $term = get_field('article_category'); ?>
                    <!-- <p class="header-content-category"><?php echo $term->name; ?></p> -->
                <?php endif; ?>
                <?php
                if ($field = get_field('header_title', $pageID)) { $title = $field; }
                else { $title = get_the_title($pageID); }
                ?>
                <?php if ($title) : ?>
                <h1 class="header-content-title"><?php echo $title; ?></h1>
                <?php endif; ?>
                <?php if (is_singular('articles')) : $desc = get_field('article_description'); ?>
                    <p class="header-content-description"><?php echo $desc; ?></p>
                <?php endif; ?>
				<?php if (function_exists(yoast_breadcrumb())) : ?>
                <?php yoast_breadcrumb('<p class="header-content-breadcrumbs">','</p>'); ?>
				<?php endif; ?>
            </div>
        </div>
    </header>
    <?php endif; ?>