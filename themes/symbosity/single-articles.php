<?php global $pageID; ?>

<?php get_header(); ?>
<section class="article container">
    <article class="article-section">
        <?php the_content(); ?> 
        <section class="bio-section">
            <div class="bio-section-heading hero-text"><?php if($heading = get_field('bio_heading', 'options')) : echo $heading; ?> <?php endif; ?></div>
            <div class="bio-section-content">
                <img class="bio-section-content-image" src="<?php if($image = get_field('bio_image', 'options')) : echo $image['url']; ?> <?php endif; ?>" />
                <div class="bio-section-content-text"><?php if($text = get_field('bio_text', 'options')) : echo $text; ?> <?php endif; ?>
                    <a class="bio-section-contact" href="mailto:<?php if($email = get_field('bio_email', 'options')) : echo $email; endif; ?>">
                        <i class="fas fa-envelope bio-section-contact-icon"></i> 
                        <p class="bio-section-contact-text">Got something to say? Contact me here</p>
                    </a>
                </div>
            </div>
        </section>
    </article>
    <div class="sidebar">
        <div class="sidebar-inner">

            <!-- <div class="article-section-content"> -->
                <ul class="article-share-list">
                    <?php 
                        $currentUrl = urlencode('https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
                        $shareUrl ="https://www.facebook.com/sharer/sharer.php?=";
                    ?>
                    <li class="article-share-list-facebook">
                        <a class="article-share-list-btn" href="<?php echo $shareUrl.$currentUrl;  ?>">
                            <span class="icon-overflow">
                                <span class="icon-container">
                                    <i class="fab fa-facebook-f"></i><span>Share</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <?php 
                        $shareUrl ="https://twitter.com/intent/tweet?text=".urlencode(get_the_title())."&url=";
                    ?>
                    <li class="article-share-list-twitter">
                        <a class="article-share-list-btn" href="<?php echo $shareUrl.$currentUrl;  ?>">
                            <span class="icon-overflow">
                                <span class="icon-container">
                                    <i class="fab fa-twitter"></i><span>Tweet</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <?php $shareUrl ="https://www.linkedin.com/shareArtile?mini=true&url="; ?>
                    <li class="article-share-list-linkedin">
                        <a class="article-share-list-btn" href="<?php echo $shareUrl.$currentUrl;  ?>">
                            <span class="icon-overflow">
                                <span class="icon-container">
                                    <i class="fab fa-linkedin-in"></i><span>Share</span>
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            <!-- </div> -->

        <button type="button" class="subscribe-btn btn">Subscribe</button>

        <div class="sidebar-social">
            <p class="sidebar-social-text">Let's connect</p>
            <div class="sidebar-social-inner">
                <?php if ($link = get_field('facebook', 'options')) : ?>
                    <a class="social-link" href="<?php echo $link; ?>"> 
                        <i class="fab fa-facebook-f"></i>   
                    </a>
                <?php endif; ?>
                <?php if ($link = get_field('youtube', 'options')) : ?>
                    <a class="social-link" href="<?php echo $link; ?>">
                        <i class="fab fa-youtube"></i>
                    </a>
                <?php endif; ?>
                <?php if ($link = get_field('twitter', 'options')) : ?>
                    <a class="social-link" href="<?php echo $link; ?>">
                        <i class="fab fa-twitter"></i>
                    </a>
                <?php endif; ?>
                <?php if ($link = get_field('linkedin', 'options')) : ?>
                    <a class="social-link" href="<?php echo $link; ?>">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                <?php endif; ?>
            </div>
        </div>

        <?php if (have_rows('chatrooms', 'options')) : ?>
        <div class="sidebar-chatrooms">
            <p class="sidebar-social-text">Join our chatrooms</p>
            <div class="sidebar-chatrooms-inner">
                <?php while (have_rows('chatrooms', 'options')) : the_row(); ?>
                <?php if ($link = get_sub_field('link')) : ?>
                <a class="sidebar-chatrooms-link" href="<?php echo $link['url']; ?>" style="--color: <?php echo get_sub_field('colour'); ?>" target="_blank">
                    <i class="fab fa-facebook"></i> <?php echo $link['title']; ?>
                </a>
                <?php endif; ?>
                <?php endwhile; ?>
            </div>
        </div>
        <?php endif; ?>

        </div>
    </div>
</section>

<?php get_footer(); ?>