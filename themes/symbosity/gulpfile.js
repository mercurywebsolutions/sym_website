const gulp = require('gulp');
const sass = require('gulp-sass');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const autoprefixer = require('gulp-autoprefixer');
const browserSync = require('browser-sync').create();
const localServer = require('./local');

var scripts = [
    './dev/js/**/*.js',
    './node_modules/swiper/js/swiper.js',
];

// compile scss into css
function style() {
    // find scss file
    return gulp.src('./dev/scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream());
}

function script() {
    return gulp.src(scripts)
        .pipe(concat('all.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist/js'));
}

function watch() {
    browserSync.init({
        proxy: 'http://localhost/' + localServer.localServer()
    });

    gulp.watch('./dev/scss/**/*.scss', style);
    gulp.watch('./**/*.php').on('change', browserSync.reload);
    gulp.watch(scripts, script).on('change', browserSync.reload);
}

exports.style = style;
exports.script = script;
exports.watch = watch;
exports.default = gulp.series(gulp.parallel(style, script), watch);