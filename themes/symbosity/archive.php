<?php global $pageID; ?>

<?php
$post_type = $post->post_type;

$category = '';
$topic = '';

if ($post_type == 'articles') {
    $pageID = get_field('article_archive_page', 'options');
    $category = 'article_categories';
} elseif ($post_type == 'videos') {
    $pageID = get_field('video_archive_page', 'options');
} elseif ($post_type == 'resources') {
    $pageID = get_field('resource_archive_page', 'options');
    $category = 'resource_categories';
    $topic = 'resource_topics';
}
?>

<?php get_header(); ?>

<section class="post-archive container">
    <?php if ($category) : $terms = get_terms(array('taxonomy' => $category)); ?>
        <div class="archive-filter">
            <p class="archive-filter-label">Filter by category:</p>
            <?php foreach ($terms as $term) : $classes = ''; $style = ''; ?>
            <?php if ($post_type == 'articles') {
                $color = get_field('category_colour', $term);

                if ($color) {
                    $style = 'style="--color:' . $color . '"';
                }

                if (isset($_GET['category'])) {
                    if ($term->slug == $_GET['category']) {
                        $classes = 'is-active';
                    }
                }
            } ?>
            <button class="archive-filter-button <?php echo $classes; ?>" <?php echo $style; ?> data-category="category-<?php echo $term->slug; ?>" type="button">
                <?php echo $term->name; ?>
            </button>            
            <?php endforeach; ?>
        </div>
    <?php endif; ?>
    <?php // not implementing at this time ?>
    <!-- <?php if ($topic) : $terms = get_terms(array('taxonomy' => $topic)); ?>
        <div class="archive-filter">
        <p class="archive-filter-label">Filter by topic:</p>
            <?php foreach ($terms as $term) : ?>
            <button class="archive-filter-button" type="button">
                <?php echo $term->name; ?>
            </button>            
            <?php endforeach; ?>
        </div>
    <?php endif; ?> -->
    <div class="post-grid">
        <?php if ($posts) : ?>
            <?php foreach ($posts as $post) : setup_postdata($post); ?>
                <?php make_post_card($post); ?>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        <?php endif; ?>
    </div>
</section>

<?php get_footer(); ?>