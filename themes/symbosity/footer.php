    <footer class="footer">
        <div class="footer-top container">
            <?php if ($text = get_field('footer_text', 'options')) : ?>
            <p class="footer-top-text"><?php echo $text; ?></p>
            <?php endif; ?>
            <div class="footer-top-social">
                <?php if ($facebook = get_field('facebook', 'options')) : ?>
                <a class="footer-top-social-link" href="<?php echo $facebook; ?>" target="_blank">
                    <i class="fab fa-facebook"></i>
                </a>
                <?php endif; ?>
                <?php if ($linkedin = get_field('linkedin', 'options')) : ?>
                <a class="footer-top-social-link" href="<?php echo $linkedin; ?>" target="_blank">
                    <i class="fab fa-linkedin"></i>
                </a>
                <?php endif; ?>
                <?php if ($twitter = get_field('twitter', 'options')) : ?>
                <a class="footer-top-social-link" href="<?php echo $twitter; ?>" target="_blank">
                    <i class="fab fa-twitter"></i>
                </a>
                <?php endif; ?>
                <?php if ($youtube = get_field('youtube', 'options')) : ?>
                <a class="footer-top-social-link" href="<?php echo $youtube; ?>" target="_blank">
                    <i class="fab fa-youtube"></i>
                </a>
                <?php endif; ?>
            </div>
        </div>
        <div class="footer-bottom container">
            <p class="footer-bottom-inner">
                Copyright &copy; Daniel Hannah <?php echo date('Y'); ?>
            </p>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
</html>