jQuery(document).ready(function ($) {
    $('.archive-filter-button').click(function () {
        $(this).siblings('.archive-filter-button').removeClass('is-active');
        $(this).toggleClass('is-active');
        doFilter();
    });

    doFilter();

    function doFilter() {
        var showCount = 0;

        $('.post-grid').find('.post-card').each(function () {
            var match = true;
            var $categoryButton = $('.archive-filter-button.is-active');

            if ($categoryButton[0] != undefined) {
                console.log($categoryButton);
                var categoryToMatch = $categoryButton.data('category');

                if (!$(this).hasClass(categoryToMatch)) {
                    match = false;
                }
            }

            $(this).toggle(match);
        });
    }
});