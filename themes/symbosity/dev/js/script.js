'use strict';

var scrollSpeed = 800;
var $navbar = jQuery('.navbar');

jQuery(document).ready(function ($) {
    OnResize();

    /// On resize of window ///
    $(window).resize(function () {
        // Call everything that must be done initially and on resize
        OnResize();
    });

    /// On scroll of window ///
    $(window).scroll(function () {
        // Background image changes colour according to how far down you scroll
        $('.home-main').css('--filter', 'hue-rotate(' + ($(window).scrollTop() / $('.home-main').outerHeight()) * -30 + 'deg)');
    });

    // Set current open page as active within navbar
    $('.menu-item a[href="' + window.location.href + '"]').parent().addClass('active');

    // Colour article links if they lead to other Symbosity articles
    $('.article-section > p > a[href*="' + window.location.hostname + '"]').css('color', '#0000bb');

    $('.subscribe-btn').click(function () {
        $('.popup').addClass('open');
    });

    $('.popup-close').click(function () {
        $('.popup').removeClass('open');
    });

    if ($('.mc4wp-response').children().length > 0) {
        $('.popup').addClass('open');
    }
});

// Initially and on resize
function OnResize() {
}