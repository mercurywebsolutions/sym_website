jQuery(document).ready(function ($) {
    // menu open/close
    $('.hamburger').click(function () {
        $('.navbar-menu').toggleClass('is-open');
        $(this).toggleClass('is-active');
    });

    $navbar = $('.navbar-main');

    previousScroll = $(document).scrollTop();
    navbarStart = parseInt($(':root').css('--navbarMenuHeight'));
    startScroll = navbarStart;
    navbarOffset = 0;
    navbarHeight = parseInt($(':root').css('--navbarMainHeight'));

    $(window).scroll(function () {
        if ($(window).width() >= 768) {
            var currentScroll = $(document).scrollTop();
            var anim = false;

            if (currentScroll == 0) {
                $navbar.css('top', navbarStart + 'px');
            } else if (currentScroll < previousScroll) {
                // scrolling up
                if (navbarOffset <= -navbarHeight) {
                    startScroll = previousScroll - navbarHeight;
                }

                anim = true;
            } else if (currentScroll > previousScroll) {
                // scrolling down
                if (navbarOffset == 0) {
                    startScroll = previousScroll;
                }

                anim = true;
            }

            if (anim) {
                navbarOffset = startScroll - currentScroll;

                if (navbarOffset < -navbarHeight) {
                    navbarOffset = -navbarHeight;
                }

                if (navbarOffset > 0) {
                    navbarOffset = 0;
                }

                $navbar.css('top', navbarStart + navbarOffset + 'px');
            }

            previousScroll = currentScroll;
        }
    });
});