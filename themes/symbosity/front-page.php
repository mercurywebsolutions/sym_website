<?php get_header(); ?>

<?php if ($background = get_field('homepage_background_image')) : ?>
<?php $background_image = $background['url']; ?>
<?php endif; ?>

<main class="home-main" style="background-image: url(<?php echo $background_image; ?>);">
    <section class="home-section hero-section">
        <div class="home-section-container hero-container container">
            <?php if ($image = get_field('homepage_featured_image')) : ?>
            <div class="hero-image">
                <img src="<?php echo $image['url']; ?>">
            </div>
            <?php endif; ?>
            <?php if ($text = get_field('homepage_text')) : ?>
            <p class="hero-text"><?php echo $text; ?></p>
            <?php endif; ?>
            <?php if ($link = get_field('homepage_link')) : ?>
            <a class="hero-link btn btn-primary" href="<?php echo $link['url']; ?>">
                <?php echo $link['title']; ?>
            </a>
            <?php endif; ?>
        </div>
    </section>

    <?php if ($terms = get_field('homepage_categories')) : ?>
        <?php foreach ($terms as $term) : $dir = get_field('homepage_orientation', $term) ?>
        <section class="home-section category-section <?php echo $dir; ?>">
            <div class="home-section-container category-container container">
                <div class="category-content">
                    <h2 class="category-heading"><?php echo $term->name; ?></h2>
                    <?php if ($text = get_field('homepage_text', $term)) : ?>
                    <p class="category-text"><?php echo $text; ?></p>
                    <?php endif; ?>
                    <?php if ($link = get_field('homepage_link')) : ?>
                    <a class="category-link btn btn-primary" href="<?php echo $link['url']; ?>?category=<?php echo $term->slug; ?>">
                        <?php echo $link['title']; ?>
                    </a>
                    <?php endif; ?>
                </div>
                <?php if ($image = get_field('homepage_featured_image', $term)) : ?>
                <div class="category-image">
                    <img src="<?php echo $image['url']; ?>">
                </div>
                <?php endif; ?>
            </div>
        </section>
        <?php endforeach; ?>
    <?php endif; ?>
</main>

<?php get_footer(); ?>