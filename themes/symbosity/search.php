<?php
global $pageID;
$pageID = get_field('search_page', 'options');
?>

<?php get_header(); ?>

<section class="post-archive container">
    <?php if ($posts) : ?>
        <div class="post-grid">
            <?php foreach ($posts as $post) : setup_postdata($post); ?>
                <?php make_post_card($post); ?>
            <?php endforeach; ?>
            <?php wp_reset_postdata(); ?>
        </div>
    <?php else : ?>
        <p>No match found! Please try again.</p>
    <?php endif; ?>
</section>

<?php get_footer(); ?>